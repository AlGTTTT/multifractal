//#include "Removing_Labels_Fr_Color_Images_2.h"
#include "Multifractal_2.h"

using namespace imago;

FILE *fout_lr;

int
	iIntensity_Interval_1_Glob,
	iIntensity_Interval_2_Glob;

int doMultifractalSpectrumOf_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);
	   
	int GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges(

		//const int nDim_2pNf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;

	float
		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot]; //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

	////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fout_lr = fopen("wMain_Multifractal.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar(); exit(1);

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		//if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			//sColor_Imagef->nIsAPixelBackground_Arr == nullptr)

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
///////////////////////////////////////////////////////////////////////////
	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	//printf("\n 4"); getchar();

	///////////////////////////////////////////////////////////////////////
	
	nRes = GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges(

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf, // float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOfFeasForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf, //float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf); // float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;

			delete[] sColor_Image.nIsAPixelBackground_Arr;
			return UNSUCCESSFUL_RETURN;
		}// if (nRes == UNSUCCESSFUL_RETURN)

		for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf];
		}//for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)

		for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2*nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iFeaf];
		}//for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2*nNumOfFeasForMultifractalTot; iFeaf++)

		for (iFeaf = 2*nNumOfFeasForMultifractalTot; iFeaf < 3 * nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf];
		}//for (iFeaf = 2*nNumOfFeasForMultifractalTot; iFeaf < 3*nNumOfFeasForMultifractalTot; iFeaf++)

		//nNumOfMultifractalFeasFor_OneDimTot

#ifndef COMMENT_OUT_ALL_PRINTS
		for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)
		{
			fprintf(fout_lr, "\n fOneDim_Multifractal_Arrf[%d] = %E", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);
		}//for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////////////////
	delete[] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sColor_Image.nLenObjectBoundary_Arr;
	delete[] sColor_Image.nIsAPixelBackground_Arr;

#ifndef COMMENT_OUT_ALL_PRINTS
	fclose(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doMultifractalSpectrumOf_ColorImage(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

int Initializing_Embedded_Image(
	const int nDim_2pNf,

	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]
{
	int
		nIndexOfPixelInEmbeddedImagef,
		nImageSizeCurf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf;

	sImageEmbeddedf_BlackWhitef->nWidth = nDim_2pNf;
	sImageEmbeddedf_BlackWhitef->nLength = nDim_2pNf;

	sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr = new int[nImageSizeCurf];

	if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		fprintf(fout_lr, "\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		 getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = -1; //invalid

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Embedded_Image(...
  ///////////////////////////////////////////////////////////////////////

int Dim_2powerN(
	const int nLengthf,
	const int nWidthf,

	int &nScalef,
	int &nDim_2pNf)
{
	int
		i,
		nTempf,
		nLargerDimInitf;

	if (nLengthf >= nWidthf)
		nLargerDimInitf = nLengthf;
	else
		nLargerDimInitf = nWidthf;

	nTempf = 2;
	for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
	{
		nTempf = nTempf * 2;
		if (nTempf >= nLargerDimInitf)
		{
			nDim_2pNf = nTempf;
			break;
		}//if (nTempf >= nLargerDimInitf)
	}//for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
/////////////////////////////////

	if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);
		fprintf(fout_lr, "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}
	else
	{
		nScalef = i;
		return SUCCESSFUL_RETURN;
	} //
}//int Dim_2powerN(...
/////////////////////////////////////////////////////////////////////////////

int Embedding_Image_Into_2powerN_ForHausdorff(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForHausdorff'");
	fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");
		printf( "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		printf( "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");

		fprintf(fout_lr, "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax)
					nIntensityAverf = nIntensityStatMax;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForHausdorff': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForHausdorff(...

//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int NumOfObjectSquaresAndLogPoint_WithResolution(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	int &nNumOfObjectSquaresTotf,
	float &fLogPointf)

{
	int
		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf* nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImageTotf, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	float
		fRatioOfSquaresf;
	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfObjectSquaresTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'NumOfObjectSquaresAndLogPoint_WithResolution'\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;

		if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
		{
			continue;
		}//if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)

		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength)
			{
				continue;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength)

			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef,nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);


#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nNumOfObjectSquaresTotf += 1;

						goto MarkContinueForiLenSquares;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

		MarkContinueForiLenSquares: continue;
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	fRatioOfSquaresf = (float)(nNumOfObjectSquaresTotf) / (float)(nNumOfSquaresInImageTotf);

	if (nNumOfObjectSquaresTotf > 1)
	{
		//fLogPointf = log(nNumOfObjectSquaresTotf) / (-log(nLenOfSquaref));
		fLogPointf = (float)(log(nNumOfObjectSquaresTotf));
	}
	else
		fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);

	fprintf(fout_lr, "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int NumOfObjectSquaresAndLogPoint_WithResolution(...
////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity --https://aip.scitation.org/doi/full/10.1063/1.4966539
int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	 int &nNumOfSquaresInImageTotf,

	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf
	int &nMassOfImageTotf, //<= 

	int nMassesOfSquaresArrf[]) //[nNumOfSquaresTotf]


{
	int
		nIndexOfSquareCurf,

		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nMassOfASquaref, 
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfNonZero_ObjectSquaresTotf = 0;
	nMassOfImageTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,
		fprintf(fout_lr, "\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare' nDim_2pNf = %d, nNumOfSquaresInImageSidef = %d, nMassOfASquareMaxf = %d", 
			nDim_2pNf, nNumOfSquaresInImageSidef, nMassOfASquareMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nIndexOfSquareCurf = iLenSquaresf + iWidSquaresf*nNumOfSquaresInImageSidef;
			if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)

			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
			{
				nMassOfASquaref = 0;
				goto MarkContinueForiLenSquares;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)

			nMassOfASquaref = 0;
			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nLenOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nMassOfASquaref += 1;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nLenOfSquareMinf
				
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

			nMassOfImageTotf += nMassOfASquaref;

		MarkContinueForiLenSquares: nMassesOfSquaresArrf[nIndexOfSquareCurf] = nMassOfASquaref;
			if (nMassOfASquaref > 0)
			{
				nNumOfNonZero_ObjectSquaresTotf += 1;
			}//if (nMassOfASquaref > 0)

			if (nMassOfASquaref > nMassOfASquareMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
				printf( "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);

				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);

				fprintf(fout_lr, "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref > nMassOfASquareMaxf)
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	if (nMassOfImageTotf < 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf = %d < 0", nMassOfImageTotf);

		fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nMassOfImageTotf = %d < 0", nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} // if (nMassOfImageTotf < 0)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);

	fprintf(fout_lr, "\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////
//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
void SlopeOfAStraightLine(
	const int nDimf,

	const float fX_Arrf[],
	const float fY_Arrf[],

	float &fSlopef)
{
	int
		i;
	float
		fX_Diff,
		fX_Averf = 0.0,
		fY_Averf = 0.0,

		fSumForNumeratorf = 0.0,
		fSumForDenominatorf = 0.0;

	for (i = 0; i < nDimf; i++)
	{
		fX_Averf += fX_Arrf[i];
		fY_Averf += fY_Arrf[i];

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SlopeOfAStraightLine' 1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine'   1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (i = 0; i < nDimf; i++)

	fX_Averf = fX_Averf / nDimf;
	fY_Averf = fY_Averf / nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);
	fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	for (i = 0; i < nDimf; i++)
	{
		fX_Diff = fX_Arrf[i] - fX_Averf;

		fSumForNumeratorf += fX_Diff * (fY_Arrf[i] - fY_Averf);

		fSumForDenominatorf += fX_Diff * fX_Diff;
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (i = 0; i < nDimf; i++)

	if (fSumForDenominatorf > 0.0)
	{
		fSlopef = fSumForNumeratorf / fSumForDenominatorf;
	}//if (fSumForDenominatorf > 0.0)
	else
		fSlopef = fLarge;

}//void SlopeOfAStraightLine(...
  ////////////////////////////////////////////////////////////////////////

int MomentOf_Q_order_AtFixedLenOfSquare(

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

	float &fSumOfMomentOf_Q_OrderAtFixedLenf,

	float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
	float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf)
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf,
		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	float PowerOfAFloatNumber(
		const int nIntOrFloatf, //1 -- int, 0 -- float
		const int nPowerf,
		const float fPowerf,

		const float fFloatInitf); //fFloatInitf != 0.0
	int
		iSquaref,

		nPowerf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nNumOfSquaresInImageTotf,
		//nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf,
		nMassOfASquaref,

		nMassOfImageTotf,
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nResf;

	float
		fMassProportionForASquaref,
		fPowerf,

		fSumOfMomentsf = 0.0, 

		fMomentOf_Q_OrderAtFixedLenCurf;
	/////////////////////////////

	if (nIntOrFloatf == 1)
	{
		nPowerf = (int)(fQf);
		fPowerf = -fLarge;
	} //if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		nPowerf = -nLarge;
		fPowerf = fQf;
	}//else if (nIntOrFloatf == 0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nIntOrFloatf = %d", nIntOrFloatf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nIntOrFloatf = %d", nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//else

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)
//////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf,nIntOrFloatf);
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf, nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
////////////////////////////////////////////////////////////////
	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf,

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfNonZero_ObjectSquaresTotMin == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",nNumOfNonZero_ObjectSquaresTotf);

		fprintf(fout_lr, "\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",	nNumOfNonZero_ObjectSquaresTotf);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fSumOfMomentOf_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)
//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
//	printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
	//	nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);

	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquare': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);

		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	///////////////////////////////////////////////////////////////
	float *fMomentsOfSquaresArrf;
	fMomentsOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsOfSquaresArrf == nullptr)

	float *fMomentsNormalizedOfSquaresArrf;
	fMomentsNormalizedOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsNormalizedOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquare'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsNormalizedOfSquaresArrf == nullptr)

	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentOf_Q_OrderAtFixedLenf = 0.0;

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			fMassProportionForASquaref = (float)(nMassOfASquaref) / (float)(nMassOfImageTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, fMassProportionForASquaref = %E, nMassOfASquaref = %d, nMassOfImageTotf = %d",
				iSquaref, fMassProportionForASquaref, nMassOfASquaref, nMassOfImageTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;

				delete[] fMomentsOfSquaresArrf;
				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)

			fMomentOf_Q_OrderAtFixedLenCurf =  PowerOfAFloatNumber(
								nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
				nPowerf, //const int nPowerf,
				fPowerf, //const float fPowerf,

				fMassProportionForASquaref); // const float fFloatInitf); //fFloatInitf != 0.0

			fSumOfMomentOf_Q_OrderAtFixedLenf += fMomentOf_Q_OrderAtFixedLenCurf;

			fMomentsOfSquaresArrf[iSquaref] = fMomentOf_Q_OrderAtFixedLenCurf; //to be normalized later

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentOf_Q_OrderAtFixedLenCurf = %E, fSumOfMomentOf_Q_OrderAtFixedLenf = %E",
				fMomentOf_Q_OrderAtFixedLenCurf, fSumOfMomentOf_Q_OrderAtFixedLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (fMomentsOfSquaresArrf[iSquaref] <= 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsOfSquaresArrf[iSquaref] <= 0.0)

		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

///////////////////////////////////////////////////////////////
//for spectrum and crowding index
	
/*
	if ( (fSumOfMomentOf_Q_OrderAtFixedLenf > -feps && fSumOfMomentOf_Q_OrderAtFixedLenf < feps) || fSumOfMomentOf_Q_OrderAtFixedLenf < 0.0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fSumOfMomentOf_Q_OrderAtFixedLenf = %E is too small", fSumOfMomentOf_Q_OrderAtFixedLenf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fSumOfMomentOf_Q_OrderAtFixedLenf = %E is too small", fSumOfMomentOf_Q_OrderAtFixedLenf);

		printf("\n\n Please press any key:"); fflush(fout_lr);  getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		delete[] fMomentsNormalizedOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if ( (fSumOfMomentOf_Q_OrderAtFixedLenf > -feps && fSumOfMomentOf_Q_OrderAtFixedLenf < feps) || fSumOfMomentOf_Q_OrderAtFixedLenf < 0.0)
*/
	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
	fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

//normalization
			fMomentsNormalizedOfSquaresArrf[iSquaref] = fMomentsOfSquaresArrf[iSquaref] / fSumOfMomentOf_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Normalization: iSquaref = %d, fMomentsNormalizedOfSquaresArrf[iSquaref] = %E, fSumOfMomentOf_Q_OrderAtFixedLenf = %E",
				iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref], fSumOfMomentOf_Q_OrderAtFixedLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (fMomentsNormalizedOfSquaresArrf[iSquaref] <= 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentsNormalizedOfSquaresArrf[%d] = %E", iSquaref,fMomentsNormalizedOfSquaresArrf[iSquaref]);
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentsNormalizedOfSquaresArrf[%d] = %E", iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref]);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsNormalizedOfSquaresArrf[iSquaref] <= 0.0)

			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref]*log (fMomentsNormalizedOfSquaresArrf[iSquaref]);

			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref] * log(fMomentsOfSquaresArrf[iSquaref]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquare': fMomentOf_Q_OrderAtFixedLenCurf = %E, fSumOfMomentOf_Q_OrderAtFixedLenf = %E, fSumOfMomentsf = %E", 
				fMomentOf_Q_OrderAtFixedLenCurf, fSumOfMomentOf_Q_OrderAtFixedLenf, fSumOfMomentsf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

//the end for spectrum and crowding index
/////////////////////////////////

	delete[] nMassesOfSquaresArrf;
	delete[] fMomentsOfSquaresArrf;

	delete[] fMomentsNormalizedOfSquaresArrf;

	return SUCCESSFUL_RETURN;
} // int MomentOf_Q_order_AtFixedLenOfSquare(...
//////////////////////////////////////////////////////////////////////

int GeneralizedDimOf_Q_order(

	const int nDim_2pNf,
		const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
	float &fGenerDim_Order_Qf,
	float &fSpectrum_Order_Qf,

	float &fCrowdingIndex_Order_Qf)
{
	int MomentOf_Q_order_AtFixedLenOfSquare(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentOf_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		fSumOfMomentOf_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		fMomentNormalizedf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],
	

		fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPointf;

	///////////////////////////////////////

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

		fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_GeneralizedDimOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)
///////////////////////
	fDiff = 1.0 - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension
	} // if (fDiff < feps && fDiff > -feps)

	///////////////////////////////////////////
	Mark_GeneralizedDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
		nLenOfSquaref = nDim_2pNf;

		fSumOfMomentsf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GeneralizedDimOf_Q_order': iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d", 
				iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquare(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentOf_Q_OrderAtFixedLenf,	// float &fSumOfMomentOf_Q_OrderAtFixedLenf);
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
				fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fMoments_Arrf[iScalef] = fSumOfMomentOf_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GeneralizedDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentOf_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d", 
			nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentOf_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf);

		fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GeneralizedDimOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points",iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			continue;
		}//if (nResf == -2)

		fSumOfMomentsf += fSumOfMomentOf_Q_OrderAtFixedLenf;

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentOf_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = log(fSumOfMomentOf_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GeneralizedDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'GeneralizedDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPointf = log( (float)(nNumOfNonZero_ObjectSquaresTotf) );
		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentOf_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPointf = fSumOfMomentOf_Q_OrderAtFixedLenf*log(fSumOfMomentOf_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPointf = 0.0;
			}//else
		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

		fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
		fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'GeneralizedDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E", 
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf,fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n\n 'GeneralizedDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'GeneralizedDimOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E", 
		nNumOfLogPointsf, iIntensity_Interval_1_Glob , iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
////////////////////////////////////////////////////////////////////////////////
	if (nNumOfLogPointsf <= 1)
	{
		fGenerDim_Order_Qf = -1.0;

		fSpectrum_Order_Qf = -1.0;
		fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'GeneralizedDimOf_Q_order': nNumOfLogPointsf = %d, fGenerDim_Order_Qf = %E", nNumOfLogPointsf,fGenerDim_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//if (nNumOfLogPointsf <= 1)
	else
	{
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

			fGenerDim_Order_Qf); // float &fSlopef);

		fDiff = 1.0 - fQf; //above

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
		} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
//////////////


		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

			fSpectrum_Order_Qf); // float &fSlopef);
///////////////////////////
	SlopeOfAStraightLine(
		nNumOfLogPointsf, //const int nDimf,

		fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
		fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

		fCrowdingIndex_Order_Qf); // float &fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'GeneralizedDimOf_Q_order': nNumOfLogPointsf = %d, fGenerDim_Order_Qf = %E, fQf = %E, fDiff = %E", 
			nNumOfLogPointsf, fGenerDim_Order_Qf, fQf, fDiff);
		
		fprintf(fout_lr, "\n fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	


	} // else
////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int GeneralizedDimOf_Q_order(...
//////////////////////////////////////////////

int GeneralizedDims_Of_All_Q_orders_AtIntensityRange(

	const int nDim_2pNf,
		const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	float fSpectrum_Of_All_Orders_Arrf[],
	float fCrowdingIndex_Of_All_Orders_Arrf[])
{
	int GeneralizedDimOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf,
		float &fSpectrum_Order_Qf,

		float &fCrowdingIndex_Order_Qf);
	int
		nResf,
		iQf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fGenerDim_Order_Qf,
		fSpectrum_Order_Qf,
		fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GeneralizedDims_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d", 
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'GeneralizedDims_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
//////////////////
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;

		fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;

		nResf = GeneralizedDimOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fGenerDim_Order_Qf, // float &fGenerDim_Order_Qf);
			fSpectrum_Order_Qf, //float &fSpectrum_Order_Qf,

			fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;
		fSpectrum_Of_All_Orders_Arrf[iQf] = fSpectrum_Order_Qf;
		fCrowdingIndex_Of_All_Orders_Arrf[iQf] = fCrowdingIndex_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n 'GeneralizedDims_Of_All_Q_orders_AtIntensityRange': fGenerDim_Of_All_Orders_Arrf[%d] = %E, fQ_Curf = %E", iQf, fGenerDim_Of_All_Orders_Arrf[iQf], fQ_Curf);
		fprintf(fout_lr, "\n\n 'GeneralizedDims_Of_All_Q_orders_AtIntensityRange': fGenerDim_Of_All_Orders_Arrf[%d] = %E, fQ_Curf = %E", iQf, fGenerDim_Of_All_Orders_Arrf[iQf], fQ_Curf);
		
		fprintf(fout_lr, "\n fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E", iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	return SUCCESSFUL_RETURN;
}// GeneralizedDims_Of_All_Q_orders_AtIntensityRange(...
//////////////////////////////////////////////////////////////

int GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges(

	//const int nDim_2pNf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

{
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); // //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int GeneralizedDims_Of_All_Q_orders_AtIntensityRange(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
		float fSpectrum_Of_All_Orders_Arrf[],
		float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1)/ nNumOfIntensity_IntervalsForMultifractal, // 8

		nNumOfIntensity_IntervalsForMultifractalTotf,
		nScalef,
		nDim_2pNf,

		iIntensity_Intervalf,

		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		iQf,

		iIndexf,
		nTempf,

		//iRowf,
		//iColf,

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	float
		fGenerDim_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fSpectrum_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fCrowdingIndex_Of_All_Orders_Arrf[nNumOf_Qs_Tot];

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
/////////////////////////////////////////////////
//initialization
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Intervalf++)
	{
		nTempf = iIntensity_Intervalf * nNumOf_Qs_Tot;
		for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
		{
			iIndexf = iQf + nTempf;

			if (iIndexf >= nProductf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				fprintf(fout_lr, "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				return UNSUCCESSFUL_RETURN;
			} //if (iIndexf >= nProductf)

			fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;

		}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	}//for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Intervalf++)

//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d", 
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
	fprintf(fout_lr, "\n\n 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d", 
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				printf( "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

			nThresholdForIntensitiesMaxf = iIntensity_Interval_2f * nLenOfIntensityIntervalf;

			fprintf(fout_lr, "\n\n 1: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]


			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);

				fprintf(fout_lr, "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

		///	fprintf(fout_lr, "\n\n2: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			//	sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

			nResf = GeneralizedDims_Of_All_Q_orders_AtIntensityRange(

				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fGenerDim_Of_All_Orders_Arrf, // float fGenerDim_Of_All_Orders_Arrf[]); //[nNumOf_Qs_Tot]
				
				fSpectrum_Of_All_Orders_Arrf, //float fSpectrum_Of_All_Orders_Arrf[],
				fCrowdingIndex_Of_All_Orders_Arrf); // float fCrowdingIndex_Of_All_Orders_Arrf[])
		//	fprintf(fout_lr, "\n\n3: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			//	sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

			nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
			for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
			{
				iIndexf = iQf + nTempf;

				fprintf(fout_lr, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, iIndexf = %d, nTempf = %d", iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iIndexf, nTempf);

				if (iIndexf >= nNumOfFeasForMultifractalTot)
				{
					printf( "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
					fprintf(fout_lr, "\n\n An error in 'GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

				}//if (iIndexf >= nNumOfFeasForMultifractalTot)

				fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fGenerDim_Of_All_Orders_Arrf[iQf];
				fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fSpectrum_Of_All_Orders_Arrf[iQf];
				fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fCrowdingIndex_Of_All_Orders_Arrf[iQf];

			}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

			//fprintf(fout_lr, "\n\n4: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	return SUCCESSFUL_RETURN;
}//int GeneralizedDims_Of_All_Q_orders_At_All_IntensityRanges(...


//////////////////////////////////////////////////////////////
float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;
	
	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
			return 1.0;
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(

  //printf("\n\n Please press any key:"); getchar();



