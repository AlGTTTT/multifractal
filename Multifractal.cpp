
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Multifractal_function.cpp"

int main()
{
	int doMultifractalSpectrumOf_ColorImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]);

	int
		nRes;

	float
		fOneDim_Multifractal_Arr[nNumOfMultifractalFeasFor_OneDimTot]; //[nNumOfMultifractalFeasFor_OneDimTot]

		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	image_in.read("Row A Day 1.png");
	//image_in.read("Row A Day 3.png");

	//image_in.read("Row B Day 1.png");
	//image_in.read("Row B Day 3.png");

	//image_in.read("Row C Day 1.png");
	//image_in.read("Row C Day 3.png");

	Image image_out;
	
	nRes = doMultifractalSpectrumOf_ColorImage(
				image_in, // Image& image_in,

		fOneDim_Multifractal_Arr); // 

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The image has not been processed properly.");

		fprintf(fout_lr,"\n\n The image has not been processed properly.");

		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Press any key to exit");	getchar(); 
		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS

	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//image_out.write("01-144_LCC_mask_NoLabels.png");

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The endof 'doMultifractalSpectrumOf_ColorImage': please press any key to exit"); getchar(); //exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();